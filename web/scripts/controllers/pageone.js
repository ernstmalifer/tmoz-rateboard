'use strict';

angular.module('pageoneApp')
.controller('pageoneCtrl', function ($scope, $http, $timeout) {

	// $scope.identity = [12,5,6,10,1,11];

	$http.get('api/rates/features').success(function(data) {
      	$scope.rates = data;
	});

	$scope.onTimeout = function(){
        $http.get('api/rates/features').success(function(data) {
	      	$scope.rates = data;
		});
        mytimeout = $timeout($scope.onTimeout,5000);
    }
    var mytimeout = $timeout($scope.onTimeout,5000);

});
