'use strict';

angular.module('pagetwoApp')
.controller('pagetwoCtrl', function ($scope, $http, $timeout) {
	$http.get('api/rates').success(function(data) {
      	$scope.rates = data;
	});

	$scope.onTimeout = function(){
        $http.get('api/rates').success(function(data) {
	      	$scope.rates = data;
		});
        mytimeout = $timeout($scope.onTimeout,5000);
    }
    var mytimeout = $timeout($scope.onTimeout,5000);

});
