'use strict';

angular.module('pageoneApp', [
  'ngResource',
  'ngRoute'
])
.config(function ($routeProvider, $locationProvider, $httpProvider) {})
.run( function ($rootScope, $location) {});

angular.module('pagetwoApp', [
  'ngResource',
  'ngRoute'
])
.config(function ($routeProvider, $locationProvider, $httpProvider) {})
.run( function ($rootScope, $location) {});

angular.module('pagethreeApp', [
  'ngResource',
  'ngRoute'
])
.config(function ($routeProvider, $locationProvider, $httpProvider) {})
.run( function ($rootScope, $location) {});