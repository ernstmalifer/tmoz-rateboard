<?php

namespace Engagis\TmozBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Rate controller.
 *
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * Lists all Rate entities.
     *
     * @Route("/pageone", name="pageone")
     * @Method("GET")
     * @Template("EngagisTmozBundle:Default:pageone.html.twig")
     */
    public function pageoneAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EngagisTmozBundle:Rate')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Lists all Rate entities.
     *
     * @Route("/pagetwo", name="pagetwo")
     * @Method("GET")
     * @Template("EngagisTmozBundle:Default:pagetwo.html.twig")
     */
    public function pagetwoAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EngagisTmozBundle:Rate')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Lists all Rate entities.
     *
     * @Route("/pagethree", name="pagethree")
     * @Method("GET")
     * @Template("EngagisTmozBundle:Default:pagethree.html.twig")
     */
    public function pagethreeAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EngagisTmozBundle:Rate')->findAll();

        return array(
            'entities' => $entities,
        );
    }


}
