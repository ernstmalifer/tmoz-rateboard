<?php

namespace Engagis\TmozBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController,
    FOS\RestBundle\Controller\Annotations\Get,
    FOS\RestBundle\Controller\Annotations\Post;

use JMS\SerializerBundle\Serializer;
use JMS\Serializer\SerializationContext;

use Symfony\Component\HttpFoundation\Response;

class ApiController extends FOSRestController
{
    /**
     * GET Route annotation.
     * @Get("/api/rates")
     */
    public function getRatesAction()
    {

        $em = $this->getDoctrine()->getManager();

        $rates = $em->getRepository('EngagisTmozBundle:Rate')
                            ->findAll();

        $serializer = $this->container->get('serializer');
        $serializeRates = $serializer->serialize($rates, 'json');

        $response = new Response();
        $response->setContent($serializeRates);
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;

    }

    /**
     * GET Route annotation.
     * @Get("/api/rates/features")
     */
    public function getFeaturedRatesAction()
    {

        $em = $this->getDoctrine()->getManager();

        $rates = $em->getRepository('EngagisTmozBundle:Rate')
                            ->findById(array(4,9,11,19,1,2), array('weight' => 'ASC'));

        $serializer = $this->container->get('serializer');
        $serializeRates = $serializer->serialize($rates, 'json');

        $response = new Response();
        $response->setContent($serializeRates);
        $response->headers->set('Content-Type', 'application/json');
        
        return $response;

    }

}