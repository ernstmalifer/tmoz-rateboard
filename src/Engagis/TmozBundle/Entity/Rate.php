<?php

namespace Engagis\TmozBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Rate
 *
 * @ORM\Table(name="rate")
 * @ORM\Entity(repositoryClass="Engagis\TmozBundle\Entity\RateRepository")
 * @ExclusionPolicy("all")
 */
class Rate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Country", type="string", length=255, nullable=true)
     * @Expose
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="Countrymi", type="string", length=255)
     * @Expose
     */
    private $countrymi;

    /**
     * @var string
     *
     * @ORM\Column(name="Currency", type="string", length=5)
     * @Expose
     */
    private $currency;

    /**
     * @var float
     *
     * @ORM\Column(name="Buying", type="float")
     * @Expose
     */
    private $buying;

    /**
     * @var float
     *
     * @ORM\Column(name="Selling", type="float")
     * @Expose
     */
    private $selling;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", nullable=true)
     * @Expose
     */
    private $weight;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Rate
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return Rate
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set buying
     *
     * @param float $buying
     * @return Rate
     */
    public function setBuying($buying)
    {
        $this->buying = $buying;

        return $this;
    }

    /**
     * Get buying
     *
     * @return float 
     */
    public function getBuying()
    {
        return $this->buying;
    }

    /**
     * Set selling
     *
     * @param float $selling
     * @return Rate
     */
    public function setSelling($selling)
    {
        $this->selling = $selling;

        return $this;
    }

    /**
     * Get selling
     *
     * @return float 
     */
    public function getSelling()
    {
        return $this->selling;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     * @return Rate
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set countrymi
     *
     * @param string $countrymi
     * @return Rate
     */
    public function setCountrymi($countrymi)
    {
        $this->countrymi = $countrymi;

        return $this;
    }

    /**
     * Get countrymi
     *
     * @return string 
     */
    public function getCountrymi()
    {
        return $this->countrymi;
    }
}
