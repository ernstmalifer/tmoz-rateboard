Flight Centre TMOZ Rateboard
========================

Welcome to Flight Centre TMOZ Rateboard Application with Symfony Standard Edition as backend and Angular on frontend

1) Installing the Application
----------------------------------

### Use Composer (*recommended*)

The app uses [Composer][1] to manage its dependencies, the recommended way
to create a new project is to use it. And [Bower][2] for client side dependencies

If you don't have Composer yet, download it following the instructions on
http://getcomposer.org/ or just run the following command:

    curl -s http://getcomposer.org/installer | php

And if you don't have Bower installed yet, Bower depends on [Node][3] and [npm][4]. It's installed globally using npm:

    npm install -g bower

And finally install the application and libraries by (*composer would handle the post updates and installs of the bower dependencies*):

    php composer.phar install


2) Checking and updating your System Configuration and Database
-------------------------------------

Make sure that your local system is properly
configured for App.

Execute the `check.php` script from the command line:

    php app/check.php

The script returns a status code of `0` if all mandatory requirements are met,
`1` otherwise.

Access the `config.php` script from a browser:

    http://localhost/path/to/symfony/app/web/config.php

If you get any warnings or recommendations, fix them before moving on.

Copy the `parameters.yml.dist` to `parameters.yml` and configure the database

    app/config/parameters.yml.dist

Execute the `console` script from the command line to generate entities:

    php app/console doctrine:generate:entities EngagisTmozBundle

Execute the `console` script from the command line to persist entities to database:

    php app/console doctrine:schama:update --force


3) Browsing the Demo Application
--------------------------------

Congratulations! You're now ready to use the app.

To see rates of the application, access the following page:

    web/rates

To see pageone of the application, access the following page:

    web/pageone

To see pagetwo of the application, access the following page:

    web/pageone

To see pagethree of the application, access the following page:

    web/pageone


[1]:  http://getcomposer.org/
[2]:  http://bower.io/
[2]:  http://nodejs.org/
[2]:  http://npmjs.org/